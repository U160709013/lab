public class Rectangle {
	int sideA;
	int sideB;
	
	Point topLeft;
	
	public Rectangle(int a, int b, Point p) {
		sideA = a;
		sideB = b;
		topLeft = p;
		
	}
	
	public int area() {
		return sideA * sideB;
	}
	
	public int perimeter() {
		return 2 * (sideA + sideB);
	}
	
	public Point[] corners() {
		Point[] corners = new Point[4];
		corners[0] = topLeft;
		Point topRight = new Point(topLeft.xCoord + sideA,topLeft.yCoord);
		Point bottomLeft = new Point(topLeft.xCoord, topLeft.yCoord + sideB);
		Point bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord + sideB);
		corners[1] = topRight;
		corners[2] = bottomLeft;
		corners[3] = bottomRight;
		return corners;
	}
}
