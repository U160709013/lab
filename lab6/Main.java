
public class Main {

	public static void main(String[] args) {
		Point p = new Point(5, 10);
		System.out.println("x = " + p.xCoord + "\ny = " + p.yCoord);

		Rectangle r = new Rectangle(6, 8, new Point(6,8));
		System.out.println("Area of rectangle : " + r.area());
		System.out.println("Perimeter of rectangle : " + r.perimeter());
		Point[] corners = r.corners();
		for(int i = 0; i < corners.length; i++) {
			System.out.println("x = " + corners[i].xCoord + " y = " + corners[i].yCoord);
		}
		
		Circle c = new Circle(5, new Point(5, 10));
		Circle c2 = new Circle(3, new Point(100,100));
		System.out.println("Area of circle : " + c.area());
		System.out.println("Perimeter of circle : " + c.perimeter());
		System.out.println("intersect : " + c.intersect(c2));
		
	}
}
