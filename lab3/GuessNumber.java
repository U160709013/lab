
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {

		Scanner reader = new Scanner(System.in); // Creates an object to read user input
		Random rand = new Random(); // Creates an object from Random class
		int number = rand.nextInt(100); // generates a number between 0 and 99
		int counter = 0;
		System.out.println(number);

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		int guess = reader.nextInt();
		
		while (guess != -1 && guess != number) {
			System.out.println("Sorry!");
			System.out.println("Type -1 to quit or guess another :" + " " + guess);
			if(guess < number){
				System.out.println("Mine is greater than yours!");
			}else {
				System.out.println("Mine is less than yours!");
			}
			guess = reader.nextInt(); // Read the user input
			counter++;
		}
		if (guess == -1) {
			System.out.println("The number was: " + number);
			reader.close();
		} else {
			System.out.println("Congraculations" + " " + "You won after " +	counter + " attempts!");
		}
		reader.close(); // Close the resource before exiting.
	}

}
