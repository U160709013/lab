import java.util.Scanner;

public class GCDLoop {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter your first value:");
		int value1 = reader.nextInt();
		System.out.println("Enter your second value:");
		int value2 = reader.nextInt();
		reader.close();
			
		System.out.println("GCD Loop10 :"  + gcd(value1,value2));
			
	}
	private static int gcd(int value1, int value2) {
		int remainder = 1;
		if (value1 == value2) {
			return value1;
		}
		
		else if (value1 > value2) {
			while(remainder != 0) {
				remainder = value1 % value2;
				value1 = value2;
				value2 = remainder;		
			}	
			return value1;

		}else {
			while(remainder != 0) {
				remainder = value2 % value1;
				value2 = value1;
				value1 = remainder;		
			}	
			return value2;			
		}
	}	
}